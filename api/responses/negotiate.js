module.exports = function negotiate(model, err) {
	var res = this.res;
	var statusCode = 500;
	var responseObj = {};
	try {
		statusCode = err.status || 500;
		res.status(statusCode);
	} catch (e) {}
	responseObj = sails.config.utils.getResponseObject (model, err);
	if (statusCode === 403) {
		return res.forbidden(responseObj);
	} else if (statusCode === 404){
		return res.notFound(responseObj);
	} else if (statusCode >= 400 && statusCode < 500) {
		return res.badRequest (responseObj);
	}
	return res.serverError (responseObj);

}