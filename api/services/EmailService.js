var mandril = require ('node-mandrill')(sails.config.constants.mandril_api_key);
module.exports = {
	sendMail:function (options) {
		var msg = {};
		msg['to'] = options['to'];
		msg['from_email'] = options['from_email'];
		msg['subject'] = options['subject'];
		msg['text'] = options['message'];
		if (options['html']) {
			msg['html'] = options['html'];
		}
		mandril('/messages/send',{message:msg}, function (err, response){

			if (err) {
				console.log (JSON.stringify (err));
			} else {
				console.log (response);
			}

		});
	}
}