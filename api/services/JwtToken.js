var nJwt = require('njwt');
var uuid = require('node-uuid');
var hashAlgorithm = "HS512";
var tokenSecret = new Buffer(sails.config.constants.jwt_secret_key).toString('base64');
module.exports = {

  issue: function(claim, time) {
    var jwt = nJwt.create(claim, tokenSecret, hashAlgorithm);
    jwt.setExpiration(new Date().getTime() + sails.config.constants.token_expiry_offset);
    return jwt.compact();

  },
  verify: function(token, callback) {
    nJwt.verify(token, tokenSecret, hashAlgorithm, function(err, verifiedJwt) {
      if (err) {
        callback(err, null, null);
      } else {
        var newToken = JwtToken.refreshToken(verifiedJwt.body);
        if (newToken) {
          callback(null, verifiedJwt, newToken);
        } else {
          callback(null, verifiedJwt, null);
        }
      }

    });
  },
  refreshToken: function(decoded) {
    var token_exp, now, newToken;
    now = new Date().getTime();
    token_exp = decoded.exp;
    token_exp *= 1000;
    if ((token_exp - now) < sails.config.constants.token_refresh_expiration * 1000) {
      var claim = {
        iss: decoded.iss,
        sub: decoded.sub,
        scope: decoded.scope
      }
      if (decoded.sub['always_logged_in']) {
        newToken = JwtToken.issue(claim, sails.config.constants.token_expiry_offset_long + new Date().getTime());
      } else {
        newToken = JwtToken.issue(claim, sails.config.constants.token_expiry_offset_short + new Date().getTime());
      }
      if (newToken) {
        return newToken;
      } else {
        return null;
      }

    } else {
      return null;
    }

  }
};