/**
 * Role.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

	attributes: {
		id: {
			type: 'integer',
			autoIncrement: true,
			primaryKey: true,
			unique: true
		},
		role_name: {
			type:'string',
			required:true
		},
		users: {
			collection:'User',
			via:'user_roles'
		},
		groups: {
			collection:'Group',
			via:'group_roles'
		}

	},
	validationMessages: {
		role_name: {
			required: 'Brand Name is required'
		}
	}
};