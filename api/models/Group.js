/**
 * Group.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */
var uuid = require('node-uuid');
module.exports = {

	attributes: {

		group_id: {
			type: 'integer',
			autoIncrement: true,
			primaryKey: true,
			unique: true
		},
		group_name: {
			type:'string',
			required:true
		},
		uuid: {
			type:'string',
			defaultsTo:uuid.v4()
		},
		users: {
			collection:'User',
			via:'groups'
		},
		group_roles: {
			collection:'Role',
			via:'groups'
		},
		companies: {
			collection:'company',
			via:'associate_company'
		}

	},
	validationMessages: {
		group_name: {
			required:'Group Name is required'
		}
	}
};