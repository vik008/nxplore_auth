/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */
var bcrypt = require('bcrypt');
var uuid = require('node-uuid');
var Q = require('q');
module.exports = {

	attributes: {
		id: {
			type: 'integer',
			primaryKey: true,
			autoIncrement: true,
			unique: true
		},
		uuid: {
			type: 'string'
		},
		name: {
			type: 'string',
			required: true
		},
		account_type: {
			type:'string',
			require:true

		},
		email: {
			type: 'email',
			required: true,
			unique: true
		},
		password: {
			type: 'string',
			required: true
		},
		personalInfo: {
			type: 'json'		
		},
		groups: {
			collection: 'Group',
			via: 'users'
		},
		company_id: {
			model: 'Company'
		},
		user_roles: {
			collection:'Role',
			via:'users'
		},
		toJSON:function () {
			var obj = this.toObject();
			delete obj.password;
			return obj;
		}

	},
	validationMessages: {
		name: {
			required: 'The name is required'
		},
		username: {
			required: 'The username is required'
		},
		email: {
			required: 'Email is required',
			email: 'Email should be valid',
			unique:'This Email has already been taken'
		},
		password: {
			required: 'The password is required'
		},
		account_type: {
			required: 'Account type is required'
		}
	},
	beforeCreate: function(user, next) {
		user['uuid'] = uuid.v4();
		User.hashPassword(user, next);
	},
	beforeUpdate: function(user, next) {
		if (user['password']) {
			User.hashPassword(user, next);
		} else {
			next();
		}
	},
	afterCreate:function (addedUser, cb) {

	},
	hashPassword: function(user, next) {
		bcrypt.genSalt(10, function(err, salt) {
			if (err) {
				return next(err);
			}
			bcrypt.hash(user.password, salt, function(err, hash) {
				if (err) {
					return next(err);
				}
				user.password = hash;
				next();
			});

		});
	},
	comparePassword: function(password, user) {
		var defered = Q.defer();
		bcrypt.compare(password, user.password, function(err, match) {
			if (err) {
				defered.reject(err);
			} else {
				defered.resolve(match);
			}

		});
		return defered.promise;

	},
	issueAndSaveToken: function(foundUser) {
		var claim = {
			iss: 'auth.nexplor.io',
			sub: foundUser,
			scope: 'god'
		}
		var token;
		if (foundUser['always_logged_in']) {
			token = JwtToken.issue(claim, sails.config.constants.token_expiry_offset_long + new Date().getTime());
		} else {
			token = JwtToken.issue(claim, sails.config.constants.token_expiry_offset_short + new Date().getTime());
		}
		var userCache = {};
		userCache['current_token'] = token;
		userCache['source'] = foundUser['source'];
		sails.config.redis_utils.insertIntoRedis(sails.config.constants.user_creds, token, userCache);
		return token;

	},
	initLogin: function(user, callback) {
		if (!user) {
			var error = {};
			error['message'] = 'Not user details provided';
			error['status'] = 400;
			return callback(error, null);
		}
		var query = User.findOne({
			email: user.email
		});
		query.exec(function(err, foundUser) {
			if (err) {
				return callback(err, null);
			} else if (!foundUser) {
				var error = {};
				error['message'] = 'User not found';
				error['status'] = 400;
				return callback(error, null);
			}
			var promise = User.comparePassword(user.password, foundUser);
			promise.then(function(match) {
				if (match) {
					foundUser['always_logged_in'] = user['always_logged_in'];
					foundUser['source'] = user['source'];
					var token = User.issueAndSaveToken(foundUser);
					foundUser['token'] = token;
					return callback(null, foundUser);
				}
				var error = {};
				error['message'] = 'Username/passoword do not match';
				error['status'] = 403;
				callback(error, null);

			});
			promise.fail(function(err) {
				var error = {};
				error['message'] = 'Something wrong happened';
				error['status'] = 500;
				return callback(err, null);
			});
		});
	}

};