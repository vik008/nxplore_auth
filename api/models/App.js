/**
 * App.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

	attributes: {
		app_id: {
			type: 'integer',
			autoIncrement: true,
			primaryKey: true
		},
		app_name: {
			type: 'string',
			required: true
		},
		app_url: {
			type: 'string',
			required: true
		},
		companies: {
			collection: 'Company',
			via: 'associated_apps'
		}
	},
	validationMessages: {
		app_name: {
			required: 'The app name is required'
		},
		app_url: {
			required: 'The app url is required'
		}
	}
};