/**
 * Company.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */
module.exports = {

	attributes: {

		company_id: {
			type: 'integer',
			autoIncrement: true,
			primaryKey: true,
			unique: true
		},
		company_name: {
			type: 'string',
			required: true
		},
		users: {
			collection: 'User',
			via: 'company_id'
		},
		associated_apps: {
			collection: 'App',
			via: 'companies'
		},
		associate_company: {
			model: 'Group',
			via: 'companies'
		}

	},
	validationMessages: {
		company_name: {
			required: 'Company Name is required'
		}

	},
	afterCreate: function(createdCompany, cb) {
		var newRole = {};
		newRole['role_name'] = "admin_" + createdCompany['company_id'];
		Role.create(newRole, function(err, newRecord) {
			if (err) {
				console.log(err);
			} else {
				console.log(newRecord);
				Company.createAcl(newRecord);
			}
			cb();

		});

	},
	createAcl: function (newRecord) {
		var acl = sails.config.acl_config.getAclInstance();
		var permissionObject = {};
			var roles = [];
			var allows = [];
			var permittedActions = {
				"resources": "auth_service",
				"permissions": []
			};
			var resource_user = {
				"resources": "user",
				"permissions": ['create', 'find', 'update', 'delete']
			};
			var resource_group = {
				"resources": "group",
				"permissions": ['create', 'find', 'update', 'delete']
			};
			var resource_role = {
				"resources":"role",
				"permissions":['create','find','update','delete']
			}
			permittedActions['permissions'].push(resource_user);
			permittedActions['permissions'].push(resource_group);
			permittedActions['permissions'].push (resource_role);
			var allowed = [];
			allows.push (permittedActions);
			roles.push (newRecord['id']);
			allowed.push ({"roles":roles,"allows":allows});
			console.log (allowed);
			acl.allow(allowed, function(err) {
				console.log("hello", err);

			});
	}
};