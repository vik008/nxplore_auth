var customBlueprintUtils = require('../CustomBlueprintUtils');
var util = require('util');
var _ = require('lodash');
module.exports = function updateOneRecord(req, res) {
  var Model = customBlueprintUtils.parseModel(req);
  var pk = customBlueprintUtils.requirePk(req);
  var values = customBlueprintUtils.parseValues(req);
  if (typeof values[Model.primaryKey] !== 'undefined' && values[Model.primaryKey] != pk) {
    req._sails.log.warn('Cannot change primary key via update blueprint; ignoring value sent for `' + Model.primaryKey + '`');
  }
  values[Model.primaryKey] = pk;
  var query = Model.findOne(pk);
  query = customBlueprintUtils.populateRequest(query, req);
  query.exec(function found(err, matchingRecord) {
    if (err) return res.serverError(err);
    if (!matchingRecord) return res.notFound();
    Model.update(pk, values).exec(function updated(err, records) {
      if (err) {
        return res.negotiate(Model, err);
      }
      if (!records || !records.length || records.length > 1) {
        req._sails.log.warn(
          util.format('Unexpected output from `%s.update`.', Model.globalId)
        );
      }
      var updatedRecord = records[0];
      if (req._sails.hooks.pubsub) {
        if (req.isSocket) {
          Model.subscribe(req, records);
        }
        Model.publishUpdate(pk, _.cloneDeep(values), !req.options.mirror && req, {
          previous: _.cloneDeep(matchingRecord.toJSON())
        });
      }
      var Q = Model.findOne(updatedRecord[Model.primaryKey]);
      Q = customBlueprintUtils.populateRequest(Q, req);
      Q.exec(function foundAgain(err, populatedRecord) {
        if (err) return res.serverError(err);
        if (!populatedRecord) return res.serverError('Could not find record after updating!');
        res.ok(populatedRecord);
      });
    });
  });
};