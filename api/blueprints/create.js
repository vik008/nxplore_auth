var customBlueprintUtils = require('../CustomBlueprintUtils');
module.exports = function createRecord (req, res) {

	var Model = customBlueprintUtils.parseModel(req);
	var data = customBlueprintUtils.parseValues(req);
	Model.create(data).exec(function created (err, newInstance) {

		if (err) {
			console.log (err);
			return res.negotiate(Model,err);
		}

		if (req._sails.hooks.pubsub) {
			if (req.isSocket) {
				Model.subscribe(req, newInstance);
				Model.introduce(newInstance);
			}
			Model.publishCreate(newInstance, !req.options.mirror && req);
		}
		res.ok(newInstance);
	});
};