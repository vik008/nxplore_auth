/**
 * sessionAuth
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
var q = require('q');
module.exports = function(req, res, next) {

	var token, source;
	token = req.param('token') || (req.body && req.body.token) || req.cookies.auth_token;
	var getTokenDetailsFromRedis = function() {
		var defer = q.defer();
		sails.config.redis_utils.getFromRedis(sails.config.constants.user_creds, token, function(err, data) {
			if (err) {
				defer.reject(err);
			} else {
				defer.resolve(data);
			}

		});
		return defer.promise;
	}

	var updateToken = function(refreshToken, data) {
		data['current_token'] = refreshToken;
		sails.config.redis_utils.insertIntoRedis(sails.config.constants.user_creds, token, data);
	}

	if (token) {
		var promise = getTokenDetailsFromRedis();
		promise.then(function(data) {
			if (data) {
				data = JSON.parse (data);
				JwtToken.verify(data['current_token'],function(err, verifiedJwt, refreshToken) {
					if (err) {
						var err = {};
						err['message'] = "Token expired";
						err['status'] = 403;
						res.negotiate(null, err);
					} else {
						if (refreshToken) {
							updateToken(refreshToken,data);
						}
						req.user_creds = verifiedJwt['body']['sub'];
						req.token = token;
						next();
					}

				});
			} else {
				var error = {};
				error['message'] = 'Not allowed to access the resource';
				error['status'] = 403;
				res.negotiate(null, error);
			}
		});

		promise.fail(function(err) {
			var error = {};
			error['message'] = 'Not allowed to access the resource';
			error['status'] = 403;
			res.negotiate(null, error);
		})

	} else {
		var error = {};
		error['message'] = 'Not allowed to access the resource';
		error['status'] = 403;
		res.negotiate(null, error);
	}
};