/**
 * AccessController
 *
 * @description :: Server-side logic for managing accesses
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	getAllResources: function(req, res) {
		var allControllers = sails.controllers;
		var controllersObj = {};
		controllersObj['service'] = "nxplore-auth";
		controllersObj['resources'] = [];
		for (var controller in allControllers) {
			var controllerDetails = {};
			controllerDetails['name'] = allControllers[controller].globalId;
			var defaultAction = ['create', 'read', 'update', 'delete'];
			controllerDetails['actions'] = defaultAction;
			for (var action in allControllers[controller]) {

				if (action === "identity") {
					break;
				}
				controllerDetails['actions'].push(action);
			}
			controllersObj['resources'].push(controllerDetails);
		}
		res.ok(controllersObj);

	}
};