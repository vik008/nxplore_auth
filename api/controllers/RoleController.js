/**
 * RoleController
 *
 * @description :: Server-side logic for managing roles
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	addRolesToUserOrGroup:function (req, res) {
		var data = req.body;
		var acl = sails.config.acl_config.getAclInstance();
		if (!data['uuid'] || !data['roles']) {
			var error = {};
			error['status'] = 400;
			error['message'] = "invalid format";
			res.forbidden (error);
		}
		acl.addUserRoles (data['uuid'], data['roles'], function (err){

			if (err) {
				console.log (err);
				var error = {};
				error['status'] = 500;
				error['message'] = "Error updating roles";
				res.forbidden(error);
			} else {
				res.ok ("roles updated successfully");
			}

		});

	}
	
};

