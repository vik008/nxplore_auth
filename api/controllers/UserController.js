
/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	login: function(req, res) {
		var data = req.body;
		if (!data['source']) {
			var err = {};
			err['status'] = 400;
			err['message'] = "Source is required";
			res.negotiate (null,err);
		}
		User.initLogin(data, function(err, responseObj) {
			if (err) {
				res.negotiate(null, err);
			} else {
				if (data['source'] === sails.config.constants.source_web && responseObj['token']) {
					var time;
					if (data['always_logged_in']) {
						time = new Date(Date.now() + sails.config.constants.token_expiry_offset_long);
					} else {
						time = new Date(Date.now() + sails.config.constants.token_expiry_offset_short);
					}
					res.cookie(sails.config.constants.auth_token, responseObj['token'], {
						expires: new Date(Date.now() + sails.config.constants.token_expiry_offset),
						httpOnly: true
					});
					delete responseObj['token'];
				}
				res.ok(responseObj);
			}

		});

	},
	forgotPassword:function (req, res) {
		var userDetails = req.user_creds;
		console.log (userDetails);
		var email = userDetails['email'];
		var options = {};
		options['message'] = "hello";
		options['to'] = [{email: 'linux.worm228@gmail.com', name: 'Jim Rubenstein'}]
		options['from'] = "noreply@nxplore.io";
		EmailService.sendMail (options);
		res.ok ('mail sent');

	},
	getUserRoles:function (req, res) {
		var uuid = req.param ('uuid');
		if (!uuid) {
			var error = {};
			error['status'] = 400;
			error['message'] = "invalid format";
			res.forbidden (error);
		}
		var acl = sails.config.acl_config.getAclInstance();
		acl.userRoles (uuid, function (err, roles){
			if (err) {
				console.log (err);
				var error = {};
				error['status'] = 500;
				error['message'] = "Internal server error";
				res.forbidden (error);
			} else {
				res.ok (roles);
			}

		});

	},
	getUserPermissions:function (req, res) {
		console.log ("hello");
		var uuid = req.param ('uuid');
		var resource = req.param ('service');
		console.log (uuid , resource);

		if (!uuid || !resource) {
			var error = {};
			error['status'] = 400;
			error['message'] = "invalid request";
			res.forbidden (error);
		}
		var acl = sails.config.acl_config.getAclInstance();
		acl.allowedPermissions (uuid,resource, function (err, obj){
			if (err) {
				var error = {};
				console.log (error);
				error['status'] = 500;
				error['message'] = "Internal Server error";
				res.forbidden (error);
			} else {
				res.ok (obj);
			}

		});

	},
	logout:function (req, res) {
		var token = req.token;
		sails.config.redis_utils.deleteFromRedis(sails.config.constants.user_creds, token);
		res.clearCookie(sails.config.constants.auth_token);
		return res.ok('Logged out succussfully');

	}


};