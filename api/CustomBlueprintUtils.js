var _ = require('lodash');
var mergeDefaults = require('merge-defaults');
var util = require('util');

var JSONP_CALLBACK_PARAM = 'callback';

var customBlueprintUtils = {


  populateRequest: function(query, req) {
    var DEFAULT_POPULATE_LIMIT = req._sails.config.blueprints.defaultLimit || 30;
    var _options = req.options;
    var aliasFilter = req.param('populate');
    var shouldPopulate = _options.populate;

    if (typeof aliasFilter === 'string') {
      aliasFilter = aliasFilter.replace(/\[|\]/g, '');
      aliasFilter = (aliasFilter) ? aliasFilter.split(',') : [];
    }

    var associations = [];

    _.each(_options.associations, function(association) {
      if (aliasFilter) {
        shouldPopulate = _.contains(aliasFilter, association.alias);
      }
      if (shouldPopulate) {
        var populationLimit =
          _options['populate_' + association.alias + '_limit'] ||
          _options.populate_limit ||
          _options.limit ||
          DEFAULT_POPULATE_LIMIT;

        associations.push({
          alias: association.alias,
          limit: populationLimit
        });
      }
    });

    return customBlueprintUtils.populateQuery(query, associations, req._sails);
  },
  populateModel: function(query, model) {
    return customBlueprintUtils.populateQuery(query, model.associations);
  },
  populateQuery: function(query, associations, sails) {
    var DEFAULT_POPULATE_LIMIT = (sails && sails.config.blueprints.defaultLimit) || 30;

    return _.reduce(associations, function(query, association) {
      return query.populate(association.alias, {
        limit: association.limit || DEFAULT_POPULATE_LIMIT
      });
    }, query);
  },
  subscribeDeep: function ( req, record ) {
    _.each(req.options.associations, function (assoc) {
      var ident = assoc[assoc.type];
      var AssociatedModel = req._sails.models[ident];

      if (req.options.autoWatch) {
        AssociatedModel.watch(req);
      }
      if (assoc.type === 'collection') {
        _.each(record[assoc.alias], function (associatedInstance) {
          AssociatedModel.subscribe(req, associatedInstance);
        });
      }
      else if (assoc.type === 'model' && record[assoc.alias]) {
        AssociatedModel.subscribe(req, record[assoc.alias]);
      }
    });
  },
  parsePk: function ( req ) {

    var pk = req.options.id || (req.options.where && req.options.where.id) || req.param('id');
    pk = _.isPlainObject(pk) ? undefined : pk;
    return pk;
  },
  requirePk: function (req) {
    var pk = module.exports.parsePk(req);
    if ( !pk ) {

      var err = new Error(
      'No `id` parameter provided.'+
      '(Note: even if the model\'s primary key is not named `id`- '+
      '`id` should be used as the name of the parameter- it will be '+
      'mapped to the proper primary key name)'
      );
      err.status = 400;
      throw err;
    }

    return pk;
  },
  parseCriteria: function ( req ) {
    req.options.criteria = req.options.criteria || {};
    req.options.criteria.blacklist = req.options.criteria.blacklist || ['limit', 'skip', 'sort', 'populate'];
    var blacklist = req.options.criteria && req.options.criteria.blacklist;
    if (blacklist && !_.isArray(blacklist)) {
      throw new Error('Invalid `req.options.criteria.blacklist`. Should be an array of strings (parameter names.)');
    }
    var where = req.params.all().where;
    if (_.isString(where)) {
      where = tryToParseJSON(where);
    }
    if (!where) {

      where = req.params.all();
      where = _.omit(where, blacklist || ['limit', 'skip', 'sort']);
      where = _.omit(where, function (p){ if (_.isUndefined(p)) return true; });
      var jsonpOpts = req.options.jsonp && !req.isSocket;
      jsonpOpts = _.isObject(jsonpOpts) ? jsonpOpts : { callback: JSONP_CALLBACK_PARAM };
      if (jsonpOpts) {
        where = _.omit(where, [jsonpOpts.callback]);
      }
    }
    where = _.merge({}, req.options.where || {}, where) || undefined;

    return where;
  },
  parseValues: function (req) {
    req.options.values = req.options.values || {};
    req.options.values.blacklist = req.options.values.blacklist;
    var blacklist = req.options.values.blacklist;
    if (blacklist && !_.isArray(blacklist)) {
      throw new Error('Invalid `req.options.values.blacklist`. Should be an array of strings (parameter names.)');
    }
    var values = [];
    if(_.isArray(req.body)){
      req._sails.log.silly("Reqest body is an array");
      _.each(req.body, function(element, key){
        values[key] = mergeDefaults(element, _.omit(req.options.values, 'blacklist'));
        values[key] = _.omit(values[key], blacklist || []);
        values[key] = _.omit(values[key], function (p){ if (_.isUndefined(p)) return true; });
      });
      return values;
    }
    values = mergeDefaults(req.params.all(), _.omit(req.options.values, 'blacklist'));
    values = _.omit(values, blacklist || []);
    values = _.omit(values, function (p){ if (_.isUndefined(p)) return true; });
    var jsonpOpts = req.options.jsonp && !req.isSocket;
    jsonpOpts = _.isObject(jsonpOpts) ? jsonpOpts : { callback: JSONP_CALLBACK_PARAM };
    if (jsonpOpts) {
      values = _.omit(values, [jsonpOpts.callback]);
    }

    return values;
  },
  parseModel: function (req) {
    var model = req.options.model || req.options.controller;
    if (!model) throw new Error(util.format('No "model" specified in route options.'));

    var Model = req._sails.models[model];
    if ( !Model ) throw new Error(util.format('Invalid route option, "model".\nI don\'t know about any models named: `%s`',model));

    return Model;
  },
  parseSort: function (req) {
    var sort = req.param('sort') || req.options.sort;
    if (typeof sort == 'undefined') {return undefined;}
    if (typeof sort == 'string') {
      try {
        sort = JSON.parse(sort);
      } catch(e) {}
    }
    return sort;
  },
  parseLimit: function (req) {
    var DEFAULT_LIMIT = req._sails.config.blueprints.defaultLimit || 30;
    var limit = req.param('limit') || (typeof req.options.limit !== 'undefined' ? req.options.limit : DEFAULT_LIMIT);
    if (limit) { limit = +limit; }
    return limit;
  },
  parseSkip: function (req) {
    var DEFAULT_SKIP = 0;
    var skip = req.param('skip') || (typeof req.options.skip !== 'undefined' ? req.options.skip : DEFAULT_SKIP);
    if (skip) { skip = +skip; }
    return skip;
  }
};
function tryToParseJSON (json) {
  if (!_.isString(json)) return null;
  try {
    return JSON.parse(json);
  }
  catch (e) { return e; }
}

module.exports = customBlueprintUtils;