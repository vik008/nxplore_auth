nxploreAuthApp.service 'LoginService',['$rootScope','$http','$cookieStore',($rootScope,$http,$cookieStore)->
	loginCredentials =
		'doLogin':(formData)->
			$http.post base_url + '/api/v0/user/login',formData

		'setCredentials':(userCreds)->
			$rootScope.globals  = {
				currentUser:userCreds
			}
			$cookieStore.put('user_creds', $rootScope.globals);

		'clearCredentials':()->
			$rootScope.globals = {}
			$cookieStore.remove('user_creds');

		'isLoggedIn':(callback) ->
			$rootScope.globals = $cookieStore.get('user_creds')
			if $rootScope.globals and !$rootScope.globals.currentUser
				callback false
			else if not $rootScope.globals
				callback false
			else
				callback true

		'saveCredentials':(creds)->
			$cookieStore.put('username', creds['email'])
			$cookieStore.put('password', creds['password'])
			$cookieStore.put('remember_me', creds['remember_me'])
			$cookieStore.put('always_logged_in',creds['always_logged_in'])

		'getKey':(key)->
			return $cookieStore.get(key)
	loginCredentials


]