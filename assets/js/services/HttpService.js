'use strict';
nxploreAuthApp.service('httpService', ['$http', '$q', function($http, $q) {

	var actions = {
		'doGetCall': function(url) {
			return $http.get(base_url + url);
		},
		'doPostCall': function(url, data) {
			return $http.post(base_url + url, data);

		}

	}
	return actions;
}]);