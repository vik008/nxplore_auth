nxploreAuthApp.controller 'TableCtrl', ['$scope', '$http', 'LoginService','$state',($scope, $http,LoginService,$state)->
	$scope.table =
		columns: []
		data: []

	successCallback = (res)->
		$scope.table.data = res.data.message
		if res.data.message and res.data.message.length
			model = res.data.message[0]
			for key of model
				$scope.table.columns.push key

	errorCallback = (res)->
		if res.status is 403
			LoginService.clearCredentials()
			$state.go 'login'

		console.error res.data.message

	$scope.getAll = (modelName, page=1, limit=30)->
		url = "/api/v0/#{modelName}/find?limit=#{limit}&skip=#{(page-1)*limit}"
		$http.get url
			.then successCallback, errorCallback
]