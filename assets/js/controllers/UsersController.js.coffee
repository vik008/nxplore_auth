class UsersCtrl extends BaseCtrl
	@register nxploreAuthApp
	@inject '$scope', '$state', '$http', 'LoginService'

	users: []

	initialize: ->
		@_getUsers()

	successCallback: (res)=>
		@users = res.data.message

	errorCallback: (res)=>
		if res.status is 403
			@LoginService.clearCredentials()
			@$state.go 'login'

	createUser: ->
		console.log 'Create new user'
		@$state.go 'dashboard.createUser'

	_getUsers: (page=1, limit=30)->
		url = "/api/v0/user/find?limit=#{limit}&skip=#{(page-1)*limit}"
		@$http.get url
			.then @successCallback, @errorCallback

UsersCtrl