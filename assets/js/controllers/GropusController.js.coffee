class GroupsCtrl extends BaseCtrl
	@register nxploreAuthApp
	@inject '$scope', '$state', '$http', 'LoginService'

	groups: []

	initialize: ->
		@_getGroups()

	successCallback: (res)=>
		@groups = res.data.message

	errorCallback: (res)=>
		if res.status is 403
			@LoginService.clearCredentials()
			@$state.go 'login'

	createGroup: ->
		console.log 'Create new group'
		@$state.go 'dashboard.createGroup'

	_getGroups: (page=1, limit=30)->
		url = "/api/v0/group/find?limit=#{limit}&skip=#{(page-1)*limit}"
		@$http.get url
			.then @successCallback, @errorCallback

GroupsCtrl