class AppsCtrl extends BaseCtrl
	@register nxploreAuthApp
	@inject '$scope', '$state', '$http', 'LoginService'

	apps: []

	initialize: ->
		@_getApps()

	successCallback: (res)=>
		@apps = res.data.message

	errorCallback: (res)=>
		if res.status is 403
			@LoginService.clearCredentials()
			@$state.go 'login'

	createApp: ->
		console.log 'Create new App'
		@$state.go 'dashboard.createApp'

	_getApps: (page=1, limit=30)->
		url = "/api/v0/app/find?limit=#{limit}&skip=#{(page-1)*limit}"
		@$http.get url
			.then @successCallback, @errorCallback
AppsCtrl