nxploreAuthApp.controller 'AddCompanyCtrl', ['$scope', '$http', ($scope, $http)->
	$scope.formData =
		brands: []
		apps: []

	$scope.brands =
		data: [
			{id: 1, label: "David"},
			{id: 2, label: "Jhon"},
			{id: 3, label: "Lisa"},
			{id: 4, label: "Nicole"},
			{id: 5, label: "Danny"},
			{id: 6, label: "Dan"},
			{id: 7, label: "Dean"},
			{id: 8, label: "Adam"},
			{id: 9, label: "Uri"},
			{id: 10, label: "Phil"}
		]

	$scope.apps =
		data: [
			{id: 1, label: "David"},
			{id: 2, label: "Jhon"},
			{id: 3, label: "Lisa"},
			{id: 4, label: "Nicole"},
			{id: 5, label: "Danny"},
			{id: 6, label: "Dan"},
			{id: 7, label: "Dean"},
			{id: 8, label: "Adam"},
			{id: 9, label: "Uri"},
			{id: 10, label: "Phil"}
		]

	multiselectSettings =
		scrollableHeight: '200px'
		scrollable: true
		enableSearch: true
		smartButtonMaxItems: 5
		smartButtonTextConverter: (itemText)-> itemText


	$scope.register = ()->
		$http.post '/api/v0/company/create', $scope.formData
			.then (data)->
				console.log data
			, (response)->
				console.error response.message

	$scope.update = (id)->
		$http.post "/api/v0/company/#{id}", $scope.formData
			.then (data)->
				console.log data
			, (response)->
				console.error response.message
]