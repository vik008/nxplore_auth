class DashboardController extends BaseCtrl
	@register nxploreAuthApp,'DashboardController'
	@inject '$scope','$state','$http','LoginService'

	successCallback: (res)=>
		@LoginService.clearCredentials()
		@$state.go 'login'

	errorCallback: (res)=>
		console.log res

	initialize: ->
		@username = @LoginService.getKey("user_creds")['currentUser']['email']
		console.log @username

	initLogout: ->
		url = base_url + '/api/v0/user/logout'
		@$http.get url
			.then @successCallback,@errorCallback

DashboardController