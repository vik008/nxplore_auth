nxploreAuthApp.controller 'LoginController', ['$scope','$http','$window','$state','LoginService', ($scope, $http, $window,$state,LoginService)->

	$scope.formData= {}
	url = base_url + '/api/v0/user/login'
	successCallBack=(response)->
		LoginService.setCredentials response.data.message
		$state.go 'dashboard'

	failureCallback=(response)->
		$window.alert response.data.message

	$scope.doLogin= ->
		$scope.formData.source = "web"
		console.log ($scope.formData);
		LoginService.saveCredentials $scope.formData
		LoginService.doLogin $scope.formData
			.then(successCallBack,failureCallback)


	$scope.onLoad= ->
		rememberMe = LoginService.getKey 'remember_me'
		if rememberMe
			$scope.formData.email = LoginService.getKey('username')
			$scope.formData.password = LoginService.getKey('password')
			$scope.formData.remember_me = rememberMe
			$scope.formData.always_logged_in = LoginService.getKey('always_logged_in')
	return undefined
]
	
