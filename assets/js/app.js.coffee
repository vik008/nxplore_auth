nxploreAuthApp = angular.module 'nxploreAuthApp', ['ngRoute', 'ui.router', 'angularjs-dropdown-multiselect','ngCookies', 'ngMaterial', 'ncy-angular-breadcrumb']
base_url = 'http://localhost:1337'
nxploreAuthApp.config ['$stateProvider', '$urlRouterProvider', '$mdIconProvider', ($stateProvider, $urlRouterProvider, $mdIconProvider)->
	$urlRouterProvider.otherwise '/'
	$stateProvider.state 'login',
		url:'/'
		templateUrl:'/templates/login.html'
		controller:'LoginController'
		authenticate:false
	.state 'dashboard',
		url: '/dashboard',
		templateUrl: '/templates/dashboard/index.html',
		authenticate:true,
		ncyBreadcrumb:
			label: 'Home page'
	.state 'dashboard.addUser',
		url: '/user/add',
		templateUrl: '/templates/dashboard/add_user.html',
		authenticate:true,
		ncyBreadcrumb:
			label: 'Users'
	.state 'dashboard.addCompany',
		url: '/company/create'
		templateUrl: '/templates/dashboard/add_company.html'
		authenticate:true
	.state 'dashboard.users',
		url: '/users',
		templateUrl: '/templates/dashboard/users.html',
		authenticate: true,
		ncyBreadcrumb:
			label: 'Users'
	.state 'dashboard.createUser',
		url: '/user/create',
		templateUrl: '/templates/dashboard/create_user.html',
		authenticate: true,
		ncyBreadcrumb:
			label: 'Create User'
			parent: 'dashboard.users'
	.state 'dashboard.groups',
		url: '/groups',
		templateUrl: '/templates/dashboard/groups.html',
		authenticate: true
	.state 'dashboard.createGroup',
		url: '/group/create',
		templateUrl: '/templates/dashboard/create_group.html',
		authenticate: true
	.state 'dashboard.apps',
		url: '/apps',
		templateUrl: '/templates/dashboard/apps.html',
		authenticate: true

	$mdIconProvider
    .iconSet('social', 'img/icons/sets/social-icons.svg', 24)
    .iconSet('device', 'img/icons/sets/device-icons.svg', 24)
    .iconSet('communication', 'img/icons/sets/communication-icons.svg', 24)
    .defaultIconSet('img/icons/sets/core-icons.svg', 24)
]

.run ['$rootScope','$location','LoginService','$state','$timeout',($rootScope,$location,LoginService,$state,$timeout)->
	$rootScope.$on '$stateChangeStart',(event,toState,toParams) ->
		LoginService.isLoggedIn (loggedIn)->
			if toState.authenticate and not loggedIn
				event.preventDefault()
				$state.go('login')
			else if toState.name is 'login' and loggedIn
				event.preventDefault()
				$state.go 'dashboard' 
			
	$rootScope.$on '$viewContentLoaded',()->
		$timeout ()->
			componentHandler.upgradeAllRegistered()
]