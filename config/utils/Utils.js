var url = require('url');
var https = require('https');
var http = require('http');
var constants = require('./Constants');
module.exports.utils = {


  parseValidationErrorObject: function(model, validationErrorObject) {
    var validationResponse = {};
    var messages = model.validationMessages;
    validationFields = Object.keys(messages);
    validationFields.forEach(function(validationField) {

      if (validationErrorObject[validationField]) {

        var FieldToProcess = validationErrorObject[validationField];
        FieldToProcess.forEach(function(rule) {

          if (messages[validationField][rule.rule]) {
            if (!(validationResponse[validationField] instanceof Array)) {

              validationResponse[validationField] = new Array();

            }
            var newMessage = {
              'rule': rule.rule,
              'message': messages[validationField][rule.rule]
            }
            validationResponse[validationField].push(newMessage);
          }

        });
      }

    });
    return validationResponse;
  },
  getResponseObject: function(model, data) {
    var responseObject = {};
    if (data.invalidAttributes && model) {
      data.invalidAttributes = this.parseValidationErrorObject(model, data.invalidAttributes);
      responseObject['message'] = data.invalidAttributes;
    } else {
      if (data['message']) {
        return data;
      } else {
        responseObject['message'] = data;
      }
    }
    return responseObject;
  },
  networkCallback: function(callback) {

    var cb = function(response) {
      var responseString = '';
      response.setEncoding('utf8');
      response.on('data', function(data) {
        responseString += data;
      });
      response.once('error', function(err) {
        callback(err, null);
      });
      response.on('end', function() {
        try {
          callback(null, JSON.parse(responseString));
        } catch (err) {
          callback(err, null);
        }

      });

    }
    return cb;

  },
  doNetworkCallHttps: function(options, callback) {
    var request = https.request(options, this.networkCallback(callback)).end();
  },
  doNetworkCallHttp: function(options, callback) {
    console.log(options);
    var request = http.request(options, this.networkCallback(callback)).end();
  }

}