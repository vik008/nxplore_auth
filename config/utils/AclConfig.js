var acl = require('acl');
var mongodb = require('mongodb');
var Constants = require('./Constants');
var backend;
mongodb.connect(Constants.constants.acl_db_path, function(err, db) {
	console.log("connecting to mongodb");
	if (err) {
		throw err;
	} else {
		backend = db;
	}

});
module.exports.acl_config = {
	getAclInstance: function() {
		console.log (backend);
		acl = new acl(new acl.mongodbBackend(backend, 'acl_'));

		return acl;
	}
}