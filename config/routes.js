
module.exports.routes = {

  'GET /api/v0/resources/get':'AccessController.getAllResources',
  'GET /api/v0/user/reset_password':'UserController.forgotPassword',
  'POST /api/v0/users/add_role':'RoleController.addRolesToUserOrGroup',
  'GET /api/v0/user/get_role':'UserController.getUserRoles',
  'GET /api/v0/user/get_permissions':'UserController.getUserPermissions'

};
